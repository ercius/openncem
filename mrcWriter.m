%mrcWriter() function
%	Description:
%		Write matrix to an MRC file. Written according to the specification at http://bio3d.colorado.edu/imod/doc/mrc_format.txt
%	Parameters:
%		arrayIn - the 3D array to be written to a file
%       fname - the name of the file to write the data to in the current directory
%       pixelSize - a 3 element vector giving the pixel size in Angstroms along each dimension of arrayIn. If a scalar is given then the program assumes all three dimensions have the same pixel size
%   Output:
%        status - The number of elements writen to disk
%	Limitations:
%		Only int16 and single (float) data types are supported
%	Author:
%		Peter Ercius 2015/12/07
function status = mrcWriter(arrayIn, fname, pixelSize)
    
    if nargin == 0
        error('No array to write');
    end
    
    %Add mrc extension if it is not already added
    [pathS, fileS, extS] = fileparts(fname);
    if ~strcmp(extS,'.mrc')
        fname = [fname,'.mrc'];
    end
    
    [fid,message]=fopen(fname,'wb');
    if fid == -1
        error(['File not available for writing: ',message]);
    end
    
    if length(pixelSize) < 3
        pixelSize = [pixelSize,pixelSize,pixelSize];
    end 
    
    dims=size(arrayIn);

    if length(dims) > 3
        ferror('too many dimensions');
    end
    
    %Initialize the file header with zeros
    fwrite(fid,zeros(256,1,'int32'),'int32');
    fseek(fid,0,'bof');
    
    %Initialize the first MRC header with zeros
    header1=zeros(10,1,'int32');

    header1(1:3)=int32(dims);

    if isa(arrayIn,'single')
        header1(4)=int32(2);
        itype = 'float32';
    %elseif isa(arrayIn,'uint16')
        %header1(4)=int32(6);
        %itype='uint16';
    elseif isa(arrayIn,'int16')
        header1(4)=int32(1);
        itype='int16';
    else
        error('Data type is unsupported. Only int16 and float32 are supported')
    end

    %Position of first column in column, row and section
    header1(5:7) = int32(0);

    %Number of intervals along X,Y,Z
    header1(8:10) = int32(dims);
    
    %Write out the first part of the header
    fwrite(fid,int32(header1),'int32');
    
    %Physical volume dimensions (in Angstroms)
    temp = size(pixelSize);

    if temp(1) ~= 1
        fwrite(fid,single(pixelSize'.*dims),'float32');
    else
        fwrite(fid,single(pixelSize.*dims),'float32');
    end

    %Cell angles (in degrees)
    fwrite(fid,single(ones(3,1).*90.0),'float32');

    %Description of array directions with respect to: Columns, Rows, Images
    %header1(17:19) = int32([1,2,3]);
    fwrite(fid,int32([1,2,3]),'int32');
    
    %Minimum and maximum density
    fwrite(fid,single([min(arrayIn(:)),max(arrayIn(:)),mean(arrayIn(:))]),'float32');
    
    %Needed to indicate that the data is little endian for NEW-STYLE MRC image2000 HEADER - IMOD 2.6.20 and above
    fseek(fid,212,'bof');
    fwrite(fid,[68,65,0,0],'char'); %use [17,17,0,0] for big endian
    
    %Write out the data after the header information
    fseek(fid,1024,'bof');
    status=fwrite(fid,arrayIn,itype);

    %Close the file
    fclose(fid);
end