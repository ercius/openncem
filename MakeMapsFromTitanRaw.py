from __future__ import division

#import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt
import numpy as np
#from PIL import Image
#from matplotlib import image
from tifffile import imsave as tiffsave
import os
import re
from io import StringIO
import csv
import glob2
import multiprocessing

#Define the maximum bit size for the images.  For example, if we want to write 16 bit tiffs, this will be 16.
MaxBitSize = 16

# -------------- Typical Energy Calibrations --------------------
# Caligula 10keV maps.
#Offset = -231.05
#Binsize = 10
# Caligula original titan maps.
#Offset = -473.6
#Binsize = 10
# Caligula 80 keV maps.
#Offset = -1889.833
#Binsize = 19.994
# Caligula 20 keV maps.
Offset = -469.147 # The first bin starts at energy offset.
Binsize = 10.001 # eV per step in energy.
# Caligula 40 keV maps.
#Offset = -945.033
#Binsize = 9.994

# -------------- KFACS FOR 80 KEV TITAN --------------------
# How many full width half maximums to use (two to go across the peak).
FWHMs = 2
# Centroid, FWHM, and correction for only reading the Kalpha peak, and the Wt % kfac, and the Element weight.
ElementBins = { 'C-K': (272, 78, 1, 2.5, 12.0107),
                'N-K': (387, 75, 1, 1.42, 14.008),
                'O-K': (521, 73, 1, 1.13, 15.9994),
                'Fe-L': (710, 88, 1, 2.055, 55.845),
                'Ni-L': (860, 88, 1, 1.530, 58.6934), # Not checked
                'Na-K': (1047, 89, 1, 0.849, 22.98976928),
                'Mg-K': (1258, 77, 1, 0.919, 24.305),
                'Al-K': (1494, 78, 1, 0.99, 26.9815386),
                'Si-K': (1745, 84, 0.99, 1, 28.0855),
                'P-K': (2015, 90, 1, 1.104, 30.973762), # Not checked
                'S-K': (2309, 98, 1, 1.062, 32.065),
                'Cl-K': (2637, 0, 1, 1.127, 35.453),
                'K-K': (3325, 0, 1, 1.177, 39.0983),
                'Ca-K': (3698, 110, 0.89, 1.274, 40.078),
                'Ti-K': (4508, 114, 0.89, 1.353, 47.867), # Not checked
                'Cr-K': (5414, 126, 0.89, 1.45, 51.9961),
                'Mn-K': (5909, 135, 0.89, 1.555, 54.938045), # Not checked
                'Fe-K': (6404, 142, 0.886, 1.618, 55.845),
                'Ni-K': (7473, 148, 0.880, 1.817, 58.6934), # Not Checked
                'Cu-K': (8042, 159, 0.873, 2.048, 63.546),
                'Zn-K': (8633, 162, 0.871, 2.209, 65.38)
                # Add your own element!
                # TODO - do a full element deconvolution with brehmsstrahlung subtraction.
                # TODO - add kfacs for 200 keV, 300 keV.
                }


def GetCalibrationFromBcf(RawName):
    Calibs = {}
    # The bcf file has the same name as the raw stack, except it ends with bcf not raw.
    with open(RawName[:-3] + 'bcf', 'r') as f:
        # Yeah, it's big but we only suck it in once.  Then we write a calibration file.
        s = f.read()
        Calibs['Width'] = int(re.search('<Width>([0-9.\-Ee]*)</Width>', s).group(1))
        Calibs['Height'] = int(re.search('<Height>([0-9.\-Ee]*)</Height>', s).group(1))
        Calibs['DX'] = float(re.search('<DX>([0-9.\-Ee]*)</DX>', s).group(1))
        Calibs['DY'] = float(re.search('<DY>([0-9.\-Ee]*)</DY>', s).group(1))
        #Calibs['ChannelCount'] = int(re.search('<ChannelCount>([0-9.\-Ee]*)</ChannelCount><Calib', s).group(1)) # Note there are multiple channel count entries.  We want one in a spectrum, which comes just before a calibration field... Yeah, hack, hack.
        Calibs['CalibAbs'] = float(re.search('<CalibAbs>([0-9.\-Ee]*)</CalibAbs>', s).group(1))*1000 # *1000 since we want eV and Bruker uses keV
        Calibs['CalibLin'] = float(re.search('<CalibLin>([0-9.\-Ee]*)</CalibLin>', s).group(1))*1000

    # #We also need the number of bites per number from the rpl file.
    Rpl = np.genfromtxt(RawName[:-3] + 'rpl', dtype='object')
    Rpl = {k:v for k,v in Rpl}
    Calibs['DataLength'] = Rpl['data-Length']
    Calibs['ChannelCount'] = Rpl['depth']

    # Write out the Calibs dictionary to a csv file.
    with open(RawName[:-3] + 'cal', 'w') as rawfile:
        f = csv.writer(rawfile)
        for k,v in Calibs.items():
                f.writerow([k,v])

def GetStack(StackName):
    # We need the dimensions and energy calibration.  Calibs will hold these data.
    Calibs = {}
    CalFile = StackName[:-3] + 'cal'

    # First look for a calibration file going along with the raw file, which has Calibs in it.
    if not os.path.isfile(CalFile):
        # If it isn't there, make it.
        GetCalibrationFromBcf(StackName)

    # Now read in the calibration file.
    with open(CalFile, 'r') as rawfile:
        f = csv.reader(rawfile)
        Calibs = {row[0]:float(row[1]) for row in f}

    # Read the raw file.
    if Calibs['DataLength'] == 1:
        stack = np.fromfile(StackName, dtype='int8')
    elif Calibs['DataLength'] == 2:
        stack = np.fromfile(StackName, dtype='int16')
    else:
        raise ValueError('Invalid data length.  Should be 1 or 2 bytes.')

    # Now reshape the stack based on the data in the calibs file.
    stack.shape = (Calibs['Height'], Calibs['Width'], Calibs['ChannelCount'])

    return stack, Calibs

def SaveTifStack(FileName, Stack):
    # tifffile will save it using the order: frames, height, width.  So we have to redimension the array.
    # Currently the array is height, width, frames.
    Stack = Stack.swapaxes(1,2)
    Stack = Stack.swapaxes(0,1)
    if Stack.dtype.itemsize > MaxBitSize/8:
        tiffsave(FileName, Stack.astype('uint'+str(MaxBitSize)))
    else:
        tiffsave(FileName, Stack)

def SaveTifImage(FileName, img):
    if img.dtype.itemsize > MaxBitSize/8:
        tiffsave(FileName, img.astype('uint'+str(MaxBitSize)))
    else:
        tiffsave(FileName, img)


def MakeMapsFromStack(StackName):

    StackPath = os.path.dirname(StackName)

    Stack, Calibs = GetStack(StackName)

    # Calibration for the energy axis from Calibs:
    Offset = Calibs['CalibAbs']
    Binsize = Calibs['CalibLin']

    # First make a sum of counts image.
    sumimage = Stack.sum(axis=-1)
    SaveTifImage(os.path.join(StackPath, 'Counts Sum.tif'), sumimage)

    # # Also show the user.
    # plt.imshow(sumimage, cmap='gray')
    # plt.show()
    #
    # exit()

    # The maps will be stored in two subdirectories.
    try:
        os.mkdir(os.path.join(StackPath, 'Maps - Count'))
    except:
        pass

    try:
        os.mkdir(os.path.join(StackPath, 'Maps - Semiquant AtPct Unnormed'))
    except:
        pass

    # Now make each semiquant image.
    for ElementName, Energies in sorted(ElementBins.iteritems()):

        # Figure out the start and end energy for this element.
        Emin = Energies[0]-Energies[1]*FWHMs/2
        Emax = Energies[0]+Energies[1]*FWHMs/2

        # Now figure out which slice corresponds to those indices.
        B1 = np.floor((Emin-Offset)/Binsize)
        B2 = np.ceil((Emax-Offset)/Binsize)

        # Inform the user.
        print('Element %s is frames %d to %d' % (ElementName, B1, B2))

        # Add up those slices:
        sumimage = Stack[:,:,B1:B2].sum(axis=-1)

        # Save this image
        SaveTifImage(os.path.join(StackPath, 'Maps - Count', ElementName + '.tif'), sumimage)

        # Compute the semiquant image: x kfac, divide correction for integrating only largest peak, divide by wt #
        semiquantimage = sumimage*Energies[3]/Energies[2]/Energies[4]

        # This image intensity is now approximately propotional to atomic percent.
        # Errors come from no brehmsstrahlung removal or accurate peak fitting.
        # For large peaks, this is correct to within 5% probably.
        # Note the values are not normalized to 100%.

        # Save this image
        SaveTifImage(os.path.join(StackPath, 'Maps - Semiquant AtPct Unnormed', ElementName + '.tif'), sumimage)

    # Finally save the tiff stack so one can open this up in imagej.
    print('Saving Stack as multiframe tif.')
    SaveTifStack(StackName[:-3] + 'tif', Stack)

def MakeMapsFromAllStacksUnderDir(DirName=None):
    import gc

    if DirName is None:
        DirName =  os.getcwd()

    #RawFiles = glob2.glob('/Users/Stardust/Desktop/TEM Cecil unprocessed/20140730 - TitanX - Cecil A6, S3 EDS' + '/**/*.raw')
    RawFiles = glob2.glob(DirName + '/**/*.raw')
    for f in RawFiles:
        print('Processing %s\n' % f)
        MakeMapsFromStack(f)
        # We are tearing through RAM with these stacks.  Sometimes, the garbage collection can't keep up and we run out of memory.
        # Or we force it to clean up after each stack.
        gc.collect()
        print('\n')

if __name__ == '__main__':
    #MakeMapsFromStack('/Users/Zack/Desktop/20150923 - TitanX - IDP Cl17,3 Grid A2 S2/Whole Section - Stack 1 - 200 kV - 1031s - 30kcps/Whole Section - Stack 1 - 200 kV - 1031s - 30kcps.raw')
    MakeMapsFromAllStacksUnderDir('/Volumes/Zack/Documents/MoonBaseBeta/ParticleBase/IDPs/IDP L2071 CL17/20150923 - TitanX - IDP Cl17,3 Grid A2 S3/Stack 3 - 200 kV - 2400s - 30kcps')
    print('Done')
