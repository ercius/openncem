function [stack3D] = K2importRAW(topDir,fileRange,crop)
% Import "dat" data from K2 camera into 3D dataset (x,y,frame)
%  - topDir: the top director that contains the Hour_00 directory just below it
%  - fileRange: a range of file numbers (e.g. 1:100). The first file in the second_00 directory is index 1. Leave empty [] to load all files.
%  - crop: [left,bottom,width,height]

% Data structure information
dirBase = fullfile(topDir,'Hour_00');
[fileList,~] = getAllFiles(dirBase);

if nargin >1 && isempty(fileRange)
    fileRange = 1:length(fileList);
end

%Test if directory contains at least as many files as requested
if fileRange(end) > length(fileList)
    error(['Error: requested files not contained in the directoy. There are only ',num2str(length(fileList)),' files in the data set.']);
end

%Set the file properties
format = 'uint16=>unit16';
xySize = [1920 1792];

%Cropping variables
if ~exist('crop','var')
    cropOrigin = [1,1];
    cropSize = xySize;
else
    cropOrigin = [crop(1) crop(2)];
    cropSize = [crop(3),crop(4)];
end

% Main loop
yv = cropOrigin(1)-1+(1:cropSize(1));
xv = cropOrigin(2)-1+(1:cropSize(2));
Nstack = [cropSize(1:2), length(fileRange)];
stack3D = zeros(Nstack(1),Nstack(2),Nstack(3),'uint16');

numFiles = length(fileRange);

%Start the progress bar
%if exist('progressbar','file')
try
    progressbar(0); %initialize progressbar
catch
    progressbar(1); %reset progressbar if necessary
    progressbar(0);
end

errorCount = 0;

%Time the import
tic
for jj = 1:numFiles
    ii = fileRange(jj);
    % Open the DM4 file
    fname = fileList{ii};
    
    fid = fopen(fname,'rb');
    dataStream = fread(fid,Inf,format);
    fclose(fid);
    
    % Reshape data into a 2D image and crop ROI        
    frame = reshape(dataStream,xySize);
    stack3D(:,:,jj) = frame(yv,xv);
    
    %Update the progress bar
    if mod(jj,5) == 0
        progressbar(jj/numFiles,2);
    end
end
toc
progressbar(1); %end progressbar if it still is open

if(errorCount > 0)
    errordlg('See command window for non existent file names.')
    disp(notExist')
end

end

function [fileList,fileSize] = getAllFiles(dirName)

  dirData = dir(dirName);      %# Get the data for the current directory
  dirIndex = [dirData.isdir];  %# Find the index for directories
  fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
  fileSize = {dirData(~dirIndex).bytes}';
  if ~isempty(fileList)
    fileList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
                       fileList,'UniformOutput',false);
  end
  subDirs = {dirData(dirIndex).name};  %# Get a list of the subdirectories
  validIndex = ~ismember(subDirs,{'.','..'});  %# Find index of subdirectories
                                               %#   that are not '.' or '..'
  for iDir = find(validIndex)                  %# Loop over valid subdirectories
    nextDir = fullfile(dirName,subDirs{iDir});    %# Get the subdirectory path
    [listTemp,sizeTemp] = getAllFiles(nextDir); %# Recursively call getAllFiles
    fileList = [fileList; listTemp];  
    fileSize = [fileSize;sizeTemp];
  end

end