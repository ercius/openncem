function dm3Conv()
% Convert all .dm3 image files in one folder to .tif (8-bit and 16-bit)
%
%P. Ercius 
% - Writes out 8-bit images (intensity 0-255) as well as 16-bit images (intensity 0 - 65,535)
% - Reads v3.0 DM3 files using dm3Reader.m
% - Outputs data for images, data cube, spectrum image (SI) w/ energy axis, and spectra

dirname = uigetdir();

DMfilenames = dir(fullfile(dirname,'*.dm3'));

%Counter to keep track of how many DM3 files are converted successfully
written = 0;

if length(DMfilenames) > 0
    [tmp1, tmp2, tmp3] = mkdir(dirname,'8bit'); %make directory for 8-bit mages
end

for ii=1:length(DMfilenames)
	
	disp(fullfile(dirname,DMfilenames(ii).name))
	
	DM = dm3Reader(fullfile(dirname,DMfilenames(ii).name));
	
	nn = strfind(DMfilenames(ii).name,'.dm3'); %get the filename only
	fname = DMfilenames(ii).name(1:nn-1);
	
	if isfield(DM,'image')
		
		imageData = DM.image;
		
		%Optimize the contrast and convert the image to 8-bit
		imageData_8 = imageData' - min(reshape(imageData,1,[]));
		imageData_8 = imageData_8./(65535.0/max(reshape(imageData_8,1,[])));
		imageData_8 = uint8((imageData_8.*255)./max(reshape(imageData_8,1,[])));
	
		%Convert to 16-bit
		imageData = uint16(imageData');

		%Write out the image in 8-bit tif format
		imwrite(imageData_8,fullfile(dirname,'8bit',[fname '.tif']),'tif');

		%Write out the 16-bit tif image
		imwrite(imageData,fullfile(dirname,[fname '.tif']),'tif');
		
		%Update counter
		written = written + 1;
	end
	
	%Spectrum image
	if isfield(DM,'SI')
		FID = fopen([fname '_SI.dat'],'wb');

		fwrite(FID,DM.SI,'float');
		en = DM.en;
		save([fname '_energy.txt'],'en','-ascii');
				
		fclose(FID);
		
		%Update counter
		written = written + 1;
	end
	
	%Spectrum
	if isfield(DM,'sig')
		out = [DM.en, DM.sig];
		save([fname '.txt'],'out','-ascii');
		
		%Update counter
		written = written + 1;
	end
	
	%Data cube without energy axis
	if isfield(DM,'cube')
        disp(['Write multi-page 8-bit tiff file for image series: ' DMfilenames(ii).name]);
        dataSize = size(DM.cube);
        for kk = 1:dataSize(3)
            if( kk == 1)
                imwrite(uint8(bytscl(DM.cube(:,:,kk))),fullfile(dirname,'8bit',[DMfilenames(ii).name(1:nn-1) '.tif']),'tif')
            else
                imwrite(uint8(bytscl(DM.cube(:,:,kk))),fullfile(dirname,'8bit',[DMfilenames(ii).name(1:nn-1) '.tif']),'tif','WriteMode','append')
            end
        end
		
		%Update counter
		written = written + 1;
	end	
end
disp(['Successfully converted ' num2str(written) ' of ' num2str(length(DMfilenames)) ' DM3 files.'])
	
end
