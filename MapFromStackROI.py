from __future__ import division

# import and use ImageJ (Java) classes as if they were Python classes
from ij import IJ
from math import *

import mpicbg.imglib.image.ImagePlusAdapter
import mpicbg.imglib.image.display.imagej.ImageJFunctions
import fiji.process.Image_Expression_Parser
from ij.io import OpenDialog

# Caligula original titan maps.
#Offset = -473.6
#Binsize = 10
# Caligula 80 keV maps.
#Offset = -1889.833
#Binsize = 19.994
# Caligula 80 keV maps.
Offset = -469.147
Binsize = 10.001

ElementBins = { 'C-K': (221, 319),
                'O-K': (470, 572),
                'Fe-L': (671, 758),
                'Na-K': (1013, 1094),
                'Mg-K': (1203, 1312),
                'Al-K': (1449, 1535),
                'Si-K': (1669, 1832),
                'P-K': (1975, 2061),
                'S-K': (2237, 2384), 
                'Cl-K': (2587, 2686),
                'K-K': (3276, 3374),
                'Ca-K': (3633, 3762),
                'Ti-K': (5353, 5474),
                'Cr-K': (5353, 5474),
                'Mn-K': (5820, 5978),
                'Fe-K': (6279, 6525),
                'Cu-K': (7910, 8176)
                } 

impStack = IJ.getImage()

for ElementName, Energies in sorted(ElementBins.iteritems()):
    B1 = floor((Energies[0]-Offset)/Binsize)
    B2 = ceil((Energies[1]-Offset)/Binsize)
    print('%d, %d' % (B1, B2))

    IJ.run("Slice Keeper", "first=" + str(B1) + "  last=" + str(B2) + " increment=1")
    imp = IJ.getImage()
    IJ.run("Z Project...", "projection=[Sum Slices]")
    imp2 = IJ.getImage()
    imp2.setTitle(ElementName)
    #IJ.showMessage(impStack.getTitle())
    imp.close()
    IJ.selectWindow(impStack.getTitle())

IJ.showMessage('Done')