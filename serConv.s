void set1DDataImage(image &dataImage,number dataType,number arrayLength){
    //Return the correct data type according to TIA's TYPE list
	
	//integerimage(name, numBytes, isSigned, size)
	//realimage(name,numBytes,size)
	if(dataType == 1) dataImage := integerimage("serReader",1,0,arrayLength) //uint8
    if(dataType == 2) dataImage := integerimage("serReader",2,0,arrayLength) //uint16
    if(dataType == 3) dataImage := integerimage("serReader",4,0,arrayLength) //uint32
    if(dataType == 4) dataImage := integerimage("serReader",1,1,arrayLength) //int8
    if(dataType == 5) dataImage := integerimage("serReader",2,1,arrayLength) //int16
    if(dataType == 6) dataImage := integerimage("serReader",4,1,arrayLength) //int32
    if(dataType == 7) dataImage := realimage("serReader",4,arrayLength) //float32
    if(dataType == 8) dataImage := realimage("serReader",8,arrayLength) //float64
    if(dataType == 12) dataImage := integerimage("serReader",4,1,arrayLength) //int32 4 byte signed integer (same as dataType==6?)
    if(dataType < 0 | dataType>12) result("Unsupported data type: " + dataType) //complex data types are currently unsupported
} //set1DDataImage()

void set2DDataImage(image &dataImage,number dataType,number arraySizeX,number arraySizeY){
    //Return the correct data type according to TIA's TYPE list

	//integerimage(name, numBytes, isSigned, sizeX, sizeY)
	//realimage(name,numBytes,sizeX,sizeY)
	if(dataType == 1) dataImage := integerimage("serReader",1,0,arraySizeX,arraySizeY) //uint8
    if(dataType == 2) dataImage := integerimage("serReader",2,0,arraySizeX,arraySizeY) //uint16
    if(dataType == 3) dataImage := integerimage("serReader",4,0,arraySizeX,arraySizeY) //uint32
    if(dataType == 4) dataImage := integerimage("serReader",1,1,arraySizeX,arraySizeY) //int8
    if(dataType == 5) dataImage := integerimage("serReader",2,1,arraySizeX,arraySizeY) //int16
    if(dataType == 6) dataImage := integerimage("serReader",4,1,arraySizeX,arraySizeY) //int32
    if(dataType == 7) dataImage := realimage("serReader",4,arraySizeX,arraySizeY) //float32
    if(dataType == 8) dataImage := realimage("serReader",8,arraySizeX,arraySizeY) //float64
    if(dataType == 12) dataImage := integerimage("serReader",4,1,arraySizeX,arraySizeY) //int32 4 byte signed integer (same as dataType==6?)
    if(dataType < 0 | dataType>12) result("Unsupported data type: " + dataType) //complex data types are currently unsupported
} //set2DDataImage()

void set3DDataImage(image &dataImage,number dataType,number arraySizeX,number arraySizeY,number arraySizeZ){
    //Return the correct data type according to TIA's TYPE list

	//integerimage(name, numBytes, isSigned, sizeX, sizeY)
	//realimage(name,numBytes,sizeX,sizeY)
	if(dataType == 1) dataImage := integerimage("serReader",1,0,arraySizeX,arraySizeY,arraySizeZ) //uint8
    if(dataType == 2) dataImage := integerimage("serReader",2,0,arraySizeX,arraySizeY,arraySizeZ) //uint16
    if(dataType == 3) dataImage := integerimage("serReader",4,0,arraySizeX,arraySizeY,arraySizeZ) //uint32
    if(dataType == 4) dataImage := integerimage("serReader",1,1,arraySizeX,arraySizeY,arraySizeZ) //int8
    if(dataType == 5) dataImage := integerimage("serReader",2,1,arraySizeX,arraySizeY,arraySizeZ) //int16
    if(dataType == 6) dataImage := integerimage("serReader",4,1,arraySizeX,arraySizeY,arraySizeZ) //int32
    if(dataType == 7) dataImage := realimage("serReader",4,arraySizeX,arraySizeY,arraySizeZ) //float32
    if(dataType == 8) dataImage := realimage("serReader",8,arraySizeX,arraySizeY,arraySizeZ) //float64
    if(dataType == 12) dataImage := integerimage("serReader",4,1,arraySizeX,arraySizeY,arraySizeZ) //int32 4 byte signed integer (same as dataType==6?)
    if(dataType < 0 | dataType>12) result("Unsupported data type: " + dataType) //complex data types are currently unsupported
} //set3DDataImage()

//Gatan script to read FEI Ser files
image serReader(string fileName){
	image imageData, spectrumData, allData
	number FID, dataType, dataTypeBIN, sizeX, sizeY
	object fileStream
	string description, units, temp
	
	FID = OpenFileForReading(fileName)

	fileStream = NewStreamFromFileReference(FID,0)
	
	image tempUInteger16 := integerImage("uint32 for reading",2,0,1)	
	image tempUInteger32 := integerImage("uint32 for reading",4,0,1)
	image tempFloat32 := RealImage("float32 for reading",4,1)
	image tempFloat64 := RealImage("float64 for reading",8,1)
	
	//Read in the header information
	image head1 := integerimage("head1",2,0,3) //name,byteSize,isSigned,xsize,ysize,...
	image head2 := integerimage("head2",4,0,6,1) //0 datatypeID, 1 tagtypeID, 2 totalNumberElements 3 validNumberElements, 4 offsetArrayOffset, 5 numberDimensions
	ImageReadImageDataFromStream( head1, fileStream, 2) //2 == little Endian
	ImageReadImageDataFromStream( head2, fileStream, 2)
	
	//Note: Getting a range from a 1D object
	//number aa := sum(head2[0,1,1,1]) //[0,left,1,right]
	
	//Head 1 variables
	number byteOrder = sum(head1[0,0]); //dec2hex(byteOrder) should be 4949
	number seriesID = sum(head1[1,0]); //%value 0x0197 indicates ES Vision Series Data File
	number seriesVersion = sum(head1[2,0]); //This can be used to test for other things as the file syntax is changed
	
	//Head2 variables
	number dataTypeID = sum(head2[0,0]); //0x4120 = 1D arrays; 0x4122 = 2D arrays
	number tagTypeID = sum(head2[1,0]); //0x4152 = Tag is time only; 0x4142 = Tag is 2D with time
    number totalNumberElements = sum(head2[2,0]); //the number of data elements in the original data set
	number validNumberElements = sum(head2[3,0]); //the number of data elements written to the file
	number offsetArrayOffset = sum(head2[4,0]); //the offset (in bytes) to the beginning of the data offset array
	number numberDimensions = sum(head2[5,0]); //the number of dimensions of the indices (not the data)

	//Dimension arrays (starts at byte offset 30)
    image dimensionSize
    image calibrationOffset
    image calibrationDelta
    image calibrationElement
    image descriptionLength
    image unitsLength

	if(numberDimensions > 0){
		dimensionSize := integerimage("dimensionSize",4,0,numberDimensions)
		calibrationOffset := realimage("calibrationOffset",8,numberDimensions)
		calibrationDelta := realimage("calibrationDelta",8,numberDimensions)
		calibrationElement := integerimage("calibrationElement",4,0,numberDimensions)
		descriptionLength := integerimage("descriptionLength",4,0,numberDimensions)
		unitsLength := integerimage("unitsLength",4,0,numberDimensions)
		for(number ij=0; ij < numberDimensions;ij++){
			ImageReadImageDataFromStream(dimensionSize[ij,0], fileStream, 2)
			ImageReadImageDataFromStream(calibrationOffset[ij,0], fileStream, 2)
			ImageReadImageDataFromStream(calibrationDelta[ij,0], fileStream, 2)
			ImageReadImageDataFromStream(calibrationElement[ij,0], fileStream, 2)
			ImageReadImageDataFromStream(descriptionLength[ij,0], fileStream, 2)
			description = readFile(FID,sum(descriptionLength[ij,0]))
			ImageReadImageDataFromStream(unitsLength[ij,0], fileStream, 2)
			temp = readFile(FID,sum(unitsLength[ij,0]))
			units.stringappend(temp+",") //I can later parse the units string in terms of commas. Spaces might also be useful.
		}
	}
    else{
		dimensionSize := integerimage("dimensionSize",4,0,1)
		calibrationOffset := realimage("calibrationOffset",8,1)
		calibrationDelta := realimage("calibrationDelta",8,1,1)
		calibrationElement := integerimage("calibrationElement",4,0,1)
		descriptionLength := integerimage("descriptionLength",4,0,1)
		unitsLength := integerimage("unitsLength",4,0,1)
	}
	
	//Go to the start of the offset arrays in the binary file
	fileStream.StreamSetPos(0,offsetArrayOffset)
		
	image dataOffsetArray
	image tagOffsetArray
	image dataOffsetArrayTemp
	image tagOffsetArrayTemp
	
	//The type of the offset array changed when TIA changed from int32 to int64 versions. Test for the SER file version number
	if(seriesVersion <= 528){ //This might need to be reduced. TIA 4.7.3 uses this series version and requires the newer dataOffsetArray reading process (int64) 
        //offsetArrayType = 'int32' for older versions of TIA before TIA 4.7.3 (Titan 1.6.4)
		dataOffsetArray := integerimage("dataOffsetArray",4,0,validNumberElements)
		ImageReadImageDataFromStream(dataOffsetArray, fileStream, 2)
		
		tagOffsetArray := integerimage("tagOffsetArray",4,0,validNumberElements)
		ImageReadImageDataFromStream(tagOffsetArray, fileStream, 2)
	}
    else{
        //offsetArrayType = 'int64' for newer versions of TIA after TIA 4.7.3 (Titan 1.6.4). DM does not have an int64 number type though. 
		dataOffsetArrayTemp := integerimage("dataOffsetArrayTemp",4,0,2*validNumberElements) //every other array value will be 0 in most cases
		ImageReadImageDataFromStream(dataOffsetArrayTemp, fileStream, 2)
		
		tagOffsetArrayTemp := integerimage("tagOffsetArrayTemp",4,0,2*validNumberElements)
		ImageReadImageDataFromStream(tagOffsetArrayTemp, fileStream, 2)
		
		//Work around since DM does not have an int64 number type. Change from 2*validnumberElements to validnumberElements
		dataOffsetArray := integerimage("dataOffsetArray",4,0,validNumberElements)
		tagOffsetArray := integerimage("tagOffsetArray",4,0,validNumberElements)
		for(number ii = 0; ii < validNumberElements;ii++){
			dataOffsetArray = dataOffsetArrayTemp[icol*2,0] //grab the even elements and assume the odd elements are 0. Conversion of int64 to int32 is then trivial
			tagOffsetArray = tagOffsetArrayTemp[icol*2,0]
			//showimage(dataOffsetArray)
		} 
	}
    
	//1D data elements datatypeID is hex: 0x4120 or decimal 16672
	if(dataTypeID == 16672){
		image calibration := realimage("calibration",8,2);
		number calibrationElement,dataType,arraySize,arrayLength,calDelta,calOffset
		//Code from python serReader.py needs to be translated:
		for(number jj=0;jj<validNumberElements;jj++){
			//f.seek(dataOffsetArray[ii],0) #seek to the correct byte offset
			fileStream.StreamSetPos(0,sum(dataOffsetArray[jj,0]))
			
			//read in the data calibration information
			//calibration = np.fromfile(f,dtype=np.float64,count=2)
			ImageReadImageDataFromStream(calibration, fileStream, 2) //reads the offset calibrationX[0] and the pixel size calibrationX[1]
			calOffset = sum(calibration[0,0])
			calDelta = sum(calibration[1,0])

			//calibrationElement = np.fromfile(f,dtype=np.int32,count=1)
			ImageReadImageDataFromStream(tempUInteger32, fileStream, 2);
			calibrationElement = sum(tempUInteger32[0,0]);

			//dataType = np.fromfile(f,dtype=np.int16,count=1)
			ImageReadImageDataFromStream(tempUInteger16, fileStream, 2);
			dataType = sum(tempUInteger16[0,0])
			
			//Type = getType(dataType) #convert SER data type to Python data type string
			//arrayLength = np.fromfile(f,dtype=np.int32,count=1)
			//dataValues = np.fromfile(f,dtype=Type,count=arrayLength)
			ImageReadImageDataFromStream(tempUInteger32, fileStream, 2);
			arrayLength = sum(tempUInteger32[0,0])
			set1DDataImage(spectrumData,dataType,arrayLength)
			ImageReadImageDataFromStream(spectrumData,fileStream,2);
			
			//Deal with 2D data sets if necessary
			if(validNumberElements>1){
				if(jj == 0) set2DdataImage(allData,dataType,arrayLength,validNumberElements) //initialize the 3D data set
				allData[jj,0,jj+1,arrayLength] = spectrumData;
			}//if(validNumberElements>1)
			else{
				allData := spectrumData
			}//else(validNumberElements>1)
            allData.setName(PathExtractFileName(fileName,0))
		}//for(jj)
		//Add calibrated data to start of spectra array
		result("serReader: Assume units are eV for EELS spectra.\n")
		ImageSetDimensionCalibration(allData,0,-calOffset/calDelta,calDelta,"eV",1)
		//ImageSetDimensionCalibration(allData,1,0,sum(calibrationDelta[0,0]),units,1)
		
	} //read 1D data sets	
	
	//2D data elements have datatypeID as hex: 0x4122 or decimal 16674
    if(dataTypeID == 16674){
        result("serReader: 2D dataset with: " + sum(head2[3,0]) + " image(s)\n")
        //Preallocate some variables
        image calibrationX := realimage("calibrationX",8,2);
		image calibrationY := realimage("calibrationY",8,2);
        image arraySize := integerimage("arraySize",4,0,2);
        number calibrationElementX,calibrationElementY,dataType,arraySizeX,arraySizeY,offsetX,calX,offsetY,calY
		
		for(number jj = 0;jj<validNumberElements;jj++){
			//f.seek(dataOffsetArray[jj],0) #seek to start of data
			fileStream.StreamSetPos(0,sum(dataOffsetArray[jj,0]))
			
			//Read calibration data for X-axis
			ImageReadImageDataFromStream(calibrationX, fileStream, 2) //reads the offset calibrationX[0] and the pixel size calibrationX[1]
			offsetX = sum(calibrationX[0,0])
			calX = sum(calibrationX[1,0])
			ImageReadImageDataFromStream(tempUInteger32, fileStream, 2);
			calibrationElementX = sum(tempUInteger32[0,0]);
			
			//Read calibration data for Y-axis
			ImageReadImageDataFromStream(calibrationY, fileStream, 2) //reads the offset calibrationX[0] and the pixel size calibrationX[1]
			offsetY = sum(calibrationY[0,0])
			calY = sum(calibrationY[1,0])
			ImageReadImageDataFromStream(tempUInteger32, fileStream, 2);
			calibrationElementY = sum(tempUInteger32[0,0]);
						
			//dataType = np.fromfile(f,dtype=np.int16,count=1)
			ImageReadImageDataFromStream(tempUInteger16, fileStream, 2);
			dataType = sum(tempUInteger16[0,0])
			
			//arraySize = np.fromfile(f,dtype=np.int32,count=2)
			//arraySizeX = arraySize[0]
			//arraySizeY = arraySize[1]
			ImageReadImageDataFromStream(arraySize, fileStream, 2);
			arraySizeX = sum(arraySize[0,0])
			arraySizey = sum(arraySize[1,0])
			//Create the image with the correct type and size
			set2DDataImage(imageData,dataType,arraySizeX,arraySizeY)
			ImageReadImageDataFromStream(imageData,fileStream,2);
			//Deal with 3D data sets if necessary
			if(validNumberElements>1){
				if(jj == 0) set3DdataImage(allData,dataType,arraySizeX,arraySizeY,validNumberElements) //initialize the 3D data set
				allData[0,0,jj,arraySizeX,arraySizeY,jj+1] = imageData;
			}
			else{
				allData := imageData
			}
            allData.setName(PathExtractFileName(fileName,0))
		} //for(validElements
		ImageSetDimensionCalibration(allData,0,-offsetX/calX,calX*1e9,"nm",1) //SER files are written in units of meters
		ImageSetDimensionCalibration(allData,1,-offsetY/calY,calY*1e9,"nm",1)
		
	}//read 2D data sets

	closefile(FID)
	return(allData)
} //serReader()

//Main script for serConv() (SER Convert)
string fileName, dirName, tLabel,fName, ext
number numFiles
number dirSuccess = OpenDialog(fileName)
image output
dirName = fileName.PathExtractDirectory(0)
result(dirName+"\n")

tagGroup tg0, tg1

if(dirSuccess){
	tg0 = GetFilesInDirectory(dirName,1)
	numFiles = tg0.TagGroupCountTags()
	result(numFiles+" total files\n")
	for(number ii = 0;ii<numFiles;ii++){
		tg0.TagGroupGetIndexedTagAsTagGroup(ii,tg1)
		tLabel = tg1.TagGroupGetTagLabel(0)
		tg1.TagGroupGetTagAsString(tLabel,fName)
		ext = PathExtractExtension(fName,0)
		if(ext=="ser"){
			result("file name: "+dirName+fName+"\n")
			output := serReader(dirName+fName)
			showimage(output)
		}
	}
}