% Read in the data in MRC format and other useful information.
% Written according to mrc specification at http://bio3d.colorado.edu/imod/doc/mrc_format.txt
% - Updated to work with FEI MRC files. Hopefully this does not break compatibility with other MRC types
%
% Output is a struct containing the following variables:
%  - stack: The data
%  - filename: The original filename for the data and metadata
%  - volumeSize: Number of pixels along X, Y, Z
%  - pixelSize: Size of pixels along each axis
%  - pixelSizeUnit: Units of the pixel size along each axis (usually Angstroms)
%  - cellAngles: Relative angles between axes (usually 90 degrees)
%  - axisOrientation: values 1,2  indicate image X, Y axes and value indicates 3 == Z axis
% Optional variables if MRC was acquired using FEI S/TEM
%  - FEIinfoString: Labels for S/TEM parameters in FEIinfo variable. Units are [meters] in relevant measurements
%  - FEIinf: S/TEM parameters at the start of the acquisition
%
% Example 1 (read in data and show first image):
% tomo1 = mrcReader('tomomgraphy.mrc');
% imagesc(tomo1.stack(:,:,1));
% Example 2 (choose mrc file in any directory and show first image):
% tomo2 = mrcReader();
% imagesc(tomo2.stack(:,:,1))
%
%Peter Ercius, 2014
function tomoData = mrcReader(fName,endian)

	fPath = '';
    
    if nargin == 0
        [fName, fPath] = uigetfile({'*.mrc;*.rec;*.st'},'Select tomography file');
    end
	
    if ~exist('endian','var')
        endian = 'n'; %use native endian (PC == little and old Mac == big)
    end
    
	if fName == 0
		error('No file opened.');
	end
	
    [FID, FIDmessage] = fopen([fPath fName],'rb');
    if FID == -1
		disp(fName)
		error(['Issue opening file: ' FIDmessage])
    end
    
    head1 = fread(FID,10,'long',0,endian);
    %tomoData.head1 = head1; %for testing only
    dataSize = head1(1:3);
    mrcType = head1(4);
    
    switch mrcType
%     mrcType     data type :
%      0        image : unsigned 8-bit bytes range -128 to 127
%      1        image : 16-bit halfwords
%      2        image : 32-bit reals
%      3        transform : complex 16-bit integers
%      4        transform : complex 32-bit reals
%      6        image : unsigned 16-bit range 0 to 65535
        case 0
            matlabType = 'uint8=>uint8';
        case 1
            matlabType = 'short=>short';
        case 2
            matlabType = 'float32=>float32';
        case 6
            matlabType = 'uint16=>uint16';
        otherwise
            error(['Unknown or unreadable MRC data type:',num2str(mrcType)])
    end
    
    %Get the pixel size for all three dimensions (in Angstroms)
    tomoData.volumeSize = fread(FID,3,'float32',0,endian);
    tomoData.pixelSize = double(tomoData.volumeSize) ./ double(dataSize);
    tomoData.pixelSizeUnit = 'Angstroms';
    
    %cell angles
    tomoData.cellAngles = fread(FID,3,'single',0,endian);
    
    %Axis orientations. Tells which axes are X,Y,Z.
    tomoData.axisOrientation = fread(FID,3,'long',0,endian);
    
    %Min, Max, Mean
    tomoData.minMaxMean = fread(FID,3,'single',0,endian);
    
    %Extra information (in FEI mrc, extra(2) is the size of the FEI information encoded with the file in terms of 4 byte floats
    extra = fread(FID,34,'long',0,endian);
    
    %Move to the end  of the normal header
    fseek(FID,1024,'bof');
    
    %Read in the extra header if it exists (for FEI MRC filetypes)
    if extra(2) ~= 0
        
        pos1 = ftell(FID);
        %disp(['extra(2) = ',num2str(extra(2))])
        
        %Read the extra FEI header if it exists
        % 1 a_tilt  first Alpha tilt (deg)
        % 2 b_tilt  first Beta tilt (deg)
        % 3 x_stage  Stage x position (Unit=m. But if value>1, unit=???m)
        % 4 y_stage  Stage y position (Unit=m. But if value>1, unit=???m)
        % 5 z_stage  Stage z position (Unit=m. But if value>1, unit=???m)
        % 6 x_shift  Image shift x (Unit=m. But if value>1, unit=???m)
        % 7 y_shift  Image shift y (Unit=m. But if value>1, unit=???m)
        % 8 defocus  starting Defocus Unit=m. But if value>1, unit=???m)
        % 9 exp_time Exposure time (s)
        % 10 mean_int Mean value of image
        % 11 tilt_axis   Tilt axis (deg)
        % 12 pixel_size  Pixel size of image (m)
        % 13 magnification   Magnification used
        % 14 ??
        % 15 ??
        tomoData.FEIinfoLabels = {'a_tilt';'b_tilt';'x_stage';'y_stage';'z_stage';'x_shift';'y_shift';'defocus';'exposure_time';'mean';'tilt_axis';'pixel_size';'magnification';'??';'??'};
        tomoData.FEIinfo = fread(FID,15,'float32',0,endian); %First 13 values are useful. Rest are zero or not known
        fseek(FID,pos1 + extra(2),'bof'); %move to the end of the standard header and non-standard header
    end
    
    %Read all of the data
    temp = fread(FID, prod(dataSize), matlabType,0,endian);
	tomoData.stack = reshape(temp,dataSize');
    
    %Store the file name
    tomoData.filename = fName;
    
    %Check that we are at the end of the file
    pos1 = ftell(FID);
    fseek(FID,0,'eof');
    if ftell(FID)-pos1 ~= 0
        disp(['Warning: Did not reach the end of the file. Data might not be correct. Off by: ', num2str(ftell(FID)-pos1), ' bytes'])
    end
    
    fclose(FID);
end